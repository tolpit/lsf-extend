var player, checkTimeout

var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {

        if(mutation.type === "childList" && !!mutation.target) {
            setTimeout(function() {
                if( !!mutation.target.querySelector('[videogular]')) {
                    if(!player || (!!player && player.video.src !== mutation.target.querySelector('[videogular] video').src)) {
                        player = new playerComponent(mutation.target.querySelector('[videogular]'))
                    }
                }
            }, 500)
        }
    })
});

var observerConfig = {
    attributes: false, 
    childList: true, 
    characterData: false 
};

observer.observe(document.body, observerConfig);

function playerComponent(player) {
    var self = this

    this.player = player;
    this.video  = player.querySelector('video')
    this.controls = player.querySelector('vg-controls table tbody tr')

    var td = document.createElement('td')
    var prevTen = document.createElement('button')
    var prevFive = document.createElement('button')

    prevFive.textContent = '-5s'
    prevTen.textContent = '-10s'

    prevFive.classList.add('btn__seek')
    prevTen.classList.add('btn__seek')

    prevTen.addEventListener('click', function() {
        self.handleSeek(-10)
    }, false)

    prevFive.addEventListener('click', function() {
        self.handleSeek(-5)
    }, false)

    td.appendChild(prevFive)
    td.appendChild(prevTen)

    td.classList.add('seeker')

    this.controls.insertBefore(td, this.controls.querySelector('td[colspan] + td + td'))
}

playerComponent.prototype.handleSeek = function(timeInSeconds) {
    // if video can be seeked
    if( !!(this.video.currentTime > 0 && this.video.readyState > 2) ) {
        var currentTime = this.video.currentTime

        this.video.currentTime = currentTime + timeInSeconds
    }    
}

playerComponent.prototype.toggle = function() {
    // is playing
    if( !!(this.video.currentTime > 0 && !this.video.paused && !this.video.ended && this.video.readyState > 2) ) {
        this.video.pause()
    }
    else {
        this.video.play()
    }
}

document.addEventListener('keydown', function(event) {
    if((!!event.code && (event.code === "Space" || event.code === "KeyK")) || (event.keyCode === 32) || (event.keyCode === 75)) {
        if(!!player) player.toggle()
    }

    // Avance de 10s
    if(event.code && event.code === "ArrowRight" && !!player) {
        player.handleSeek(10)
    }

    // Retour en arrière de 10s
    if(event.code && event.code === "ArrowLeft" && !!player) {
        player.handleSeek(-10)
    } 
})